// https://vuex.vuejs.org/en/mutations.html

export default {
  SET_DRAWER: (state, payload) => (state.appDrawer = payload),
  TOGGLE_DRAWER: state => (state.appDrawer = !state.appDrawer),
  LOADING: state => (state.loading = !state.loading),
  DARK: state => (state.dark = !state.dark)
}
