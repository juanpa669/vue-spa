// https://vuex.vuejs.org/en/state.html

export default {
  appDrawer: true,
  loading: false,
  dark: false
}
