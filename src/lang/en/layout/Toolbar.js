export default {
  items: [
    {
      to: '/',
      text: 'Home'
    },
    {
      to: '/about',
      text: 'About'
    },
    {
      to: '/portfolio',
      text: 'Portfolio'
    },
    {
      to: '/contact',
      text: 'Contacts'
    }
  ]
}
