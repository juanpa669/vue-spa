export default {
  jumbotronTitle: 'Portfolio',
  jumbotronSubTitle: '',
  callToActionBtn: 'Démarrer un projet',
  platformHeader: 'Réalisation de site sur mesure'
}
