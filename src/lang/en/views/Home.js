export default {
  jumbotronTitle: 'Freelance Developer',
  jumbotronSubTitle: 'La solution à tous vos problèmes informatiques.',
  callToActionBtn: 'Demande de support',
  platformHeader: 'Dépannage informatique dans les Comminges',
  testimonials: [
    {
      author: 'Elisabeth Marnier',
      title: 'Muret',
      quote: '"Très disponible, rapide et sympas ! Moi qui ne suis pas très calée en informatique, Jean-François a pu solutionner tout mes problèmes et m\'a même donner de bons conseils."'
    },
    {
      author: 'Michel Tillon',
      title: 'Saint-Bertrand-de-Comminges',
      quote: '"Je cherchais à monter un nouvel ordinateur qui correspondrait à mes besoins, Jean-François s\'est révélé être de très bon conseil et m\'a fait économiser sur la facture finale."'
    }
  ]
}
