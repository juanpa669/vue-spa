export default {
  jumbotronTitle: 'Contact',
  jumbotronSubTitle: 'Besoin d`\'un rensignement ?',
  heading1: 'Nous sommes à votre écoute !',
  headingText1: 'Pour toute demande d\'information, veuillez remplir le formulaire et le soumettre',
  name: '',
  phone: '',
  address: '',
  cityState: '',
  zip: ' ',
  submit: 'Soumettre'
}
