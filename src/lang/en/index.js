// http://kazupon.github.io/vue-i18n/en/messages.html
import Layout from './layout'
import Views from './views'

export default {
  Layout,
  Views,
  title: 'Freelance Developer',
  footer: 'Yellow Monkeys Studio',
  drawerItems: ['Be Inspire'],
  needHelp: 'Need help?'
}
