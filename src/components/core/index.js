import AppDrawer from './AppDrawer.vue'
import AppFooter from './AppFooter.vue'
import AppToolbar from './AppToolbar.vue'
import AppView from './AppView.vue'
import AppJumbotron from './AppJumbotron'
import AppWeather from './AppWeather'

export default {
  AppJumbotron,
  AppWeather,
  AppDrawer,
  AppFooter,
  AppToolbar,
  AppView
}
