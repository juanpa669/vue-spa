import MacflyCarousel from './MacflyCarousel.vue'
import MacflyCube from './MacflyCube.vue'

export default {
  MacflyCarousel,
  MacflyCube
}
