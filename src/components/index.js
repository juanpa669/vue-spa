// Exported components will be automatically
// bootstrapped into your application
// e.g. import MyComponent from './MyComponent'

import AlphaComponents from './alpha'
import CoreComponent from './core'
import MacComponent from './macfly'

export default {
  ...AlphaComponents,
  ...CoreComponent,
  ...MacComponent
  // MyComponent
}
