/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
export default [
  {
    path: '',
    // Relative to /src/views
    view: 'Home',
    meta: {
      title: 'Vuetify Alpha',
      description: 'Customized vue-cli templates for Vue and Vuetify',
      keywords: 'vue, vuetify, vuetify templates, vuetify themes'
    }
  },
  {
    path: 'about',
    // Relative to /src/views
    view: 'About',
    meta: {
      title: 'Vuetify Alpha',
      description: 'Customized vue-cli templates for Vue and Vuetify',
      keywords: 'vue, vuetify, vuetify templates, vuetify themes'
    }
  },
  {
    path: 'contact',
    // Relative to /src/views
    view: 'Contact',
    meta: {
      title: 'Vuetify Alpha',
      description: 'Customized vue-cli templates for Vue and Vuetify',
      keywords: 'vue, vuetify, vuetify templates, vuetify themes'
    }
  },
  {
    path: 'portfolio',
    // Relative to /src/views
    view: 'Portfolio',
    meta: {
      title: 'Vuetify Alpha',
      description: 'Customized vue-cli templates for Vue and Vuetify',
      keywords: 'vue, vuetify, vuetify templates, vuetify themes'
    }
  }
]
